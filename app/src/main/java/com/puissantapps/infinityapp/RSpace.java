package com.puissantapps.infinityapp;

import java.util.ArrayList;

/**
 * Created by gdev on 6/22/16.
 */
public class RSpace implements Constants {

    public static final ArrayList<String> mAllPackages = new ArrayList<>();

    static {
        mAllPackages.add(PKG_BUG_SMASHER_FREE);
        mAllPackages.add(PKG_WORD_SEARCH_FREE);
        mAllPackages.add(PKG_UNBLOCK_IT_FREE);
        mAllPackages.add(PKG_CAT_RESCUE_FREE);
        mAllPackages.add(PKG_TRAFFIC_HR_FREE);
        mAllPackages.add(PKG_PLUMBER_MANIA);
        mAllPackages.add(PKG_BASKET_BALL_FREE);
        mAllPackages.add(PKG_ARROW_SHOOTER_FREE);
        mAllPackages.add(PKG_PLUMBER_10K_FREE);
        mAllPackages.add(PKG_FIT_IT_FREE);
        mAllPackages.add(PKG_DUCK_HUNTER_FREE);
        mAllPackages.add(PKG_EGG_CATCH_FREE);
        mAllPackages.add(PKG_WS_MOVIES_FREE);
    }


    public static void main(String[] args) {

//
//        for (String pp : mAllPackages) {
//            File f = new File("adDir/" + pp);
//            f.mkdir();
//        }


    }
}
