package com.puissantapps.infinityapp;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;

import com.puissantapps.xpromo.Infinity;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Infinity.initialize(this, Infinity.LANDSCAPE);
        Infinity.getInstance().setDebuggable(true);

    }

    public void launchExit(View view) {
        Infinity.getInstance().showExitDialog(this);
    }

    public void launchMoreGames(View view) {
        Infinity.getInstance().showMoreGames();

    }

    public void launchFullAd(View view) {
        Infinity.getInstance().showFullScreenAd();
    }

}
