package com.puissantapps.infinityapp;

import android.annotation.TargetApi;
import android.os.Build;
import android.support.v4.util.Pair;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.io.FileWriter;
import java.io.IOException;
import java.io.Writer;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by gdev on 6/21/16.
 */
public class RackSpaceJSON {

    public static final String EXT = ".json";

    public static final String DIR = "adFiles/";

    //public static final String PKG_TILES_MATCH = "com.puissantapps.tilematch";
    public static final String PKG_BUG_SMASHER_FREE = "com.puissantapps.bugsmasher.free";
    public static final String PKG_WORD_SEARCH_FREE = "com.puissantapps.wordsearch.game";
    public static final String PKG_UNBLOCK_IT_FREE = "com.puissantapps.unblockit.free";
    public static final String PKG_1TOUCH_BUMPY = "com.onetouch.bumpy";
    public static final String PKG_CAT_RESCUE_FREE = "com.puissantapps.catrescue.free";
    public static final String PKG_TRAFFIC_HR_FREE = "com.puissantapps.trafficjam.free";
    public static final String PKG_PLUMBER_MANIA = "com.puissantapps.plumbermania.free";
    public static final String PKG_BASKET_BALL_FREE = "com.puissantapps.basketball.free";
    public static final String PKG_ARROW_SHOOTER_FREE = "com.puissantapps.arrow.free";
    public static final String PKG_PLUMBER_10K_FREE = "com.puissantapps.plumbergame.free";
    public static final String PKG_FIT_IT_FREE = "com.puissantapps.fititpuzzles.free";
    public static final String PKG_DUCK_HUNTER_FREE = "com.puissantapps.duckhunter.free";
    public static final String PKG_EGG_CATCH_FREE = "com.puissantapps.eggcatch.free";
    public static final String PKG_WS_MOVIES_FREE = "com.puissantapps.wordsearch.movies";
    //public static final String PKG_1TCH_WORD_GAME = "com.voyager.games.playwordsearch";
    //public static final String PKG_EGG_DROP_GAME = "com.puissantapps.dropbird";
    //public static final String PKG_1TOUCH_SUDOKU = "com.onetouch.sudokuplay";
    public static final String PKG_1TOUCH_PIANO = "com.onetouch.pianoball";

    //public static final String NAME_TILES_MATCH = "Tiles Match";
    public static final String NAME_BUG_SMASHER_FREE = "Bug Smasher";
    public static final String NAME_WORD_SEARCH_FREE = "Word Search";
    public static final String NAME_UNBLOCK_IT_FREE = "Unblock It";
    public static final String NAME_CAT_RESCUE_FREE = "Cat Rescue";
    public static final String NAME_TRAFFIC_HR_FREE = "Traffic Hour";
    public static final String NAME_PLUMBER_MANIA = "Plumber Mania";
    public static final String NAME_BASKET_BALL_FREE = "Basket Ball";
    public static final String NAME_ARROW_SHOOTER_FREE = "Arrow Shooter";
    public static final String NAME_PLUMBER_10K_FREE = "Plumber 10k";
    public static final String NAME_FIT_IT_FREE = "Fit It Puzzles";
    public static final String NAME_DUCK_HUNTER_FREE = "Duck Hunter";
    public static final String NAME_EGG_CATCH_FREE = "Egg Catcher";
    public static final String NAME_WS_MOVIES_FREE = "Word Search Movies";
    //public static final String NAME_1TOUCH_BUMPY = "Bumpy Clash";
    //public static final String NAME_1TOUCH_SUDOKU = "Sudoku";
    //public static final String NAME_EGG_DROP_FREE = "Egg Drop";
    public static final String NAME_1TOUCH_PIANO = "Piano Ball";

    private static final ArrayList<Pair<String, String>> ALL_ICON_GAMES = new ArrayList<>();
    private static final ArrayList<Pair<String, String>> ALL_GAMES = new ArrayList<>();
    private static final ArrayList<FullAd> FULL_ADS_LIST_WITH_PRIORITY = new ArrayList<>();

    static {
        //ALL_ICON_GAMES.add(new Pair<>(NAME_EGG_DROP_FREE, PKG_EGG_DROP_GAME));
        ALL_ICON_GAMES.add(new Pair<>(NAME_BUG_SMASHER_FREE, PKG_BUG_SMASHER_FREE));
        ALL_ICON_GAMES.add(new Pair<>(NAME_WORD_SEARCH_FREE, PKG_WORD_SEARCH_FREE));
        ALL_ICON_GAMES.add(new Pair<>(NAME_TRAFFIC_HR_FREE, PKG_TRAFFIC_HR_FREE));
        ALL_ICON_GAMES.add(new Pair<>(NAME_PLUMBER_MANIA, PKG_PLUMBER_MANIA));
        ALL_ICON_GAMES.add(new Pair<>(NAME_BASKET_BALL_FREE, PKG_BASKET_BALL_FREE));
        ALL_ICON_GAMES.add(new Pair<>(NAME_ARROW_SHOOTER_FREE, PKG_ARROW_SHOOTER_FREE));
        ALL_ICON_GAMES.add(new Pair<>(NAME_FIT_IT_FREE, PKG_FIT_IT_FREE));
        ALL_ICON_GAMES.add(new Pair<>(NAME_EGG_CATCH_FREE, PKG_EGG_CATCH_FREE));
        ALL_ICON_GAMES.add(new Pair<>(NAME_UNBLOCK_IT_FREE, PKG_UNBLOCK_IT_FREE));
        //ALL_ICON_GAMES.add(new Pair<>(NAME_1TOUCH_BUMPY, PKG_1TOUCH_BUMPY));
        //ALL_ICON_GAMES.add(new Pair<>(NAME_1TOUCH_PIANO, PKG_1TOUCH_PIANO));
        //ALL_ICON_GAMES.add(new Pair<>(NAME_TILES_MATCH, PKG_TILES_MATCH));

    }

    static {
        //ALL_GAMES.add(new Pair<>(NAME_EGG_DROP_FREE, PKG_EGG_DROP_GAME));
        ALL_GAMES.add(new Pair<>(NAME_BUG_SMASHER_FREE, PKG_BUG_SMASHER_FREE));
        ALL_GAMES.add(new Pair<>(NAME_WORD_SEARCH_FREE, PKG_WORD_SEARCH_FREE));
        ALL_GAMES.add(new Pair<>(NAME_CAT_RESCUE_FREE, PKG_CAT_RESCUE_FREE));
        ALL_GAMES.add(new Pair<>(NAME_TRAFFIC_HR_FREE, PKG_TRAFFIC_HR_FREE));
        ALL_GAMES.add(new Pair<>(NAME_PLUMBER_MANIA, PKG_PLUMBER_MANIA));
        ALL_GAMES.add(new Pair<>(NAME_BASKET_BALL_FREE, PKG_BASKET_BALL_FREE));
        ALL_GAMES.add(new Pair<>(NAME_ARROW_SHOOTER_FREE, PKG_ARROW_SHOOTER_FREE));
        ALL_GAMES.add(new Pair<>(NAME_PLUMBER_10K_FREE, PKG_PLUMBER_10K_FREE));
        ALL_GAMES.add(new Pair<>(NAME_FIT_IT_FREE, PKG_FIT_IT_FREE));
        ALL_GAMES.add(new Pair<>(NAME_DUCK_HUNTER_FREE, PKG_DUCK_HUNTER_FREE));
        ALL_GAMES.add(new Pair<>(NAME_EGG_CATCH_FREE, PKG_EGG_CATCH_FREE));
        ALL_GAMES.add(new Pair<>(NAME_UNBLOCK_IT_FREE, PKG_UNBLOCK_IT_FREE));
        ALL_GAMES.add(new Pair<>(NAME_WS_MOVIES_FREE, PKG_WS_MOVIES_FREE));
        //ALL_GAMES.add(new Pair<>(NAME_1TOUCH_BUMPY, PKG_1TOUCH_BUMPY));
        ALL_GAMES.add(new Pair<>(NAME_1TOUCH_PIANO, PKG_1TOUCH_PIANO));
        //ALL_GAMES.add(new Pair<>(NAME_TILES_MATCH, PKG_TILES_MATCH));
        //ALL_GAMES.add(new Pair<>(NAME_WORD_SEARCH_FREE, PKG_1TCH_WORD_GAME));
    }

    static {
        //FULL_ADS_LIST_WITH_PRIORITY.add(new FullAd(PKG_EGG_DROP_GAME, "#ED1E79"));

        FULL_ADS_LIST_WITH_PRIORITY.add(new FullAd(PKG_BUG_SMASHER_FREE, "#0E50CA"));
        FULL_ADS_LIST_WITH_PRIORITY.add(new FullAd(PKG_WORD_SEARCH_FREE, "#ED1E79"));
        FULL_ADS_LIST_WITH_PRIORITY.add(new FullAd(PKG_UNBLOCK_IT_FREE, "#0E50CA"));
        FULL_ADS_LIST_WITH_PRIORITY.add(new FullAd(PKG_1TOUCH_PIANO, "#0E50CA"));
        //FULL_ADS_LIST_WITH_PRIORITY.add(new FullAd(PKG_1TOUCH_BUMPY, "#ED1E79"));
        //FULL_ADS_LIST_WITH_PRIORITY.add(new FullAd(PKG_1TCH_WORD_GAME, "#0E50CA"));
        //FULL_ADS_LIST_WITH_PRIORITY.add(new FullAd(PKG_TILES_MATCH, "#ED1E79"));

        FULL_ADS_LIST_WITH_PRIORITY.clear();

    }

    @TargetApi(Build.VERSION_CODES.KITKAT)
    public static void main(String[] args) {

        ArrayList<AdInfo> mAds = new ArrayList<>();
        for (Pair p1 : ALL_GAMES) {

            List<FullAd> fullScreenAds = new ArrayList<>();
            List<IconAd> iconAds = new ArrayList<>();

            for (FullAd p2 : FULL_ADS_LIST_WITH_PRIORITY) {
                if (!p1.second.equals(p2.getPackageName())) {
                    fullScreenAds.add(p2);
                }
            }


            for (Pair<String, String> p2 : ALL_ICON_GAMES) {
                if (!p1.second.equals(p2.second)) {
                    iconAds.add(new IconAd(p2.first, p2.second));
                }
            }

            AdInfo adInfo = new AdInfo(true, (String) p1.second, AdInfo.EXIT_AD_ICONS, fullScreenAds, iconAds, true, true);
            mAds.add(adInfo);

            try (Writer writer = new FileWriter(DIR + adInfo.getPackageName() + EXT)) {
                Gson gson = new GsonBuilder().create();
                gson.toJson(adInfo, writer);
            } catch (IOException e) {
                e.printStackTrace();
            }

        }

        System.out.println(new Gson().toJson(mAds));

    }

    private static class FullAd {

        String packageName;

        String buttonColor;

        public FullAd(String packageName, String buttonColor) {
            this.packageName = packageName;
            this.buttonColor = buttonColor;
        }

        public String getPackageName() {
            return packageName;
        }

        public void setPackageName(String packageName) {
            this.packageName = packageName;
        }

        public String getButtonColor() {
            return buttonColor;
        }

        public void setButtonColor(String buttonColor) {
            this.buttonColor = buttonColor;
        }
    }

    private static class IconAd {


        String packageName;

        String name;

        public IconAd(String name, String packageName) {
            this.name = name;
            this.packageName = packageName;
        }

        public String getPackageName() {
            return packageName;
        }

        public void setPackageName(String packageName) {
            this.packageName = packageName;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }
    }

    static class AdInfo {

        public static final String EXIT_AD_NONE = "none";

        public static final String EXIT_AD_ICONS = "icons";

        public static final String EXIT_AD_FULL = "full";

        boolean enabled;

        boolean shuffleIconAds;

        boolean shuffleFullAds;

        String packageName;

        String exitAd;

        List<FullAd> fullAds = new ArrayList<>();

        List<IconAd> iconAds = new ArrayList<>();


        public AdInfo(boolean enabled, String packageName, String exitAd, List<FullAd> fullAds,
                      List<IconAd> iconAds, boolean shuffleIconAds, boolean shuffleFullAds) {
            this.enabled = enabled;
            this.packageName = packageName;
            this.exitAd = exitAd;
            this.fullAds = fullAds;
            this.iconAds = iconAds;
            this.shuffleIconAds = shuffleIconAds;
            this.shuffleFullAds = shuffleFullAds;
        }

        public boolean isEnabled() {
            return enabled;
        }

        public void setEnabled(boolean enabled) {
            this.enabled = enabled;
        }

        public String getPackageName() {
            return packageName;
        }

        public void setPackageName(String packageName) {
            this.packageName = packageName;
        }

        public List<FullAd> getFullAds() {
            return fullAds;
        }

        public void setFullAds(List<FullAd> fullAds) {
            this.fullAds = fullAds;
        }

        public String getExitAd() {
            return exitAd;
        }

        public void setExitAd(String exitAd) {
            this.exitAd = exitAd;
        }

        public List<IconAd> getIconAds() {
            return iconAds;
        }

        public void setIconAds(List<IconAd> iconAds) {
            this.iconAds = iconAds;
        }
    }
}
