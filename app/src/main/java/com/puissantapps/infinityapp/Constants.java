package com.puissantapps.infinityapp;

/**
 * Created by gdev on 6/22/16.
 */
public interface Constants {


    String PKG_BUG_SMASHER_FREE = "com.puissantapps.bugsmasher.free";
    String PKG_WORD_SEARCH_FREE = "com.puissantapps.wordsearch.game";
    String PKG_UNBLOCK_IT_FREE = "com.puissantapps.unblockit.free";
    String PKG_CAT_RESCUE_FREE = "com.puissantapps.catrescue.free";
    String PKG_TRAFFIC_HR_FREE = "com.puissantapps.trafficjam.free";
    String PKG_PLUMBER_MANIA = "com.puissantapps.plumbermania.free";
    String PKG_BASKET_BALL_FREE = "com.puissantapps.basketball.free";
    String PKG_ARROW_SHOOTER_FREE = "com.puissantapps.arrow.free";
    String PKG_PLUMBER_10K_FREE = "com.puissantapps.plumbergame.free";
    String PKG_FIT_IT_FREE = "com.puissantapps.fititpuzzles.free";
    String PKG_DUCK_HUNTER_FREE = "com.puissantapps.duckhunter.free";
    String PKG_EGG_CATCH_FREE = "com.puissantapps.eggcatch.free";
    String PKG_WS_MOVIES_FREE = "com.puissantapps.wordsearch.movies";

    String NAME_BUG_SMASHER_FREE = "Bug Smasher";
    String NAME_WORD_SEARCH_FREE = "Word Search";
    String NAME_UNBLOCK_IT_FREE = "Unblock It";
    String NAME_CAT_RESCUE_FREE = "Cat Rescue";
    String NAME_TRAFFIC_HR_FREE = "Traffic Hour";
    String NAME_PLUMBER_MANIA = "Plumber Mania";
    String NAME_BASKET_BALL_FREE = "Basket Ball";
    String NAME_ARROW_SHOOTER_FREE = "Arrow Shooter";
    String NAME_PLUMBER_10K_FREE = "Plumber 10k";
    String NAME_FIT_IT_FREE = "Fit It Puzzles";
    String NAME_DUCK_HUNTER_FREE = "Duck Hunter";
    String NAME_EGG_CATCH_FREE = "Egg Catcher";
    String NAME_WS_MOVIES_FREE = "Word Search Movies";


    String COLOR_BLUE = "#0E50CA";
    String COLOR_PINK = "#ED1E79";

    String COLOR_DEFAULT = COLOR_BLUE;

    class GameInfo {

        String name;

        String packageName;

        String buttonColor;

    }




}
