package com.puissantapps.infinityapp;

import android.support.v4.util.Pair;

import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * Created by gdev on 6/20/16.
 */
public class JSONCreator {


    public static final String BUG_SMASHER_FREE = "com.puissantapps.bugsmasher.free";
    public static final String WORD_SEARCH_FREE = "com.puissantapps.wordsearch.game";
    public static final String UNBLOCK_IT_FREE = "com.puissantapps.unblockit.free";

    public static final String NAME_BUG_SMASHER_FREE = "Bug Smasher";
    public static final String NAME_WORD_SEARCH_FREE = "Word Search";
    public static final String NAME_UNBLOCK_IT_FREE = "Unblock It";

    private static ArrayList<Pair<String, String>> mAllGames = new ArrayList<>();

    static {
        mAllGames.add(new Pair<>(NAME_BUG_SMASHER_FREE, BUG_SMASHER_FREE));
        mAllGames.add(new Pair<>(NAME_WORD_SEARCH_FREE, WORD_SEARCH_FREE));
        mAllGames.add(new Pair<>(NAME_UNBLOCK_IT_FREE, UNBLOCK_IT_FREE));
    }


    static class GameInfo {
        String packageName;

        String name;

        public GameInfo(String packageName, String name) {
            this.packageName = packageName;
            this.name = name;
        }
    }

    static class Game {

        GameInfo gameInfo;

        AdInfo adInfo;

        public Game(GameInfo gameInfo, AdInfo adInfo) {
            this.gameInfo = gameInfo;
            this.adInfo = adInfo;
        }

        public GameInfo getGameInfo() {
            return gameInfo;
        }

        public void setGameInfo(GameInfo gameInfo) {
            this.gameInfo = gameInfo;
        }

        public AdInfo getAdInfo() {
            return adInfo;
        }

        public void setAdInfo(AdInfo adInfo) {
            this.adInfo = adInfo;
        }
    }


    static class AdInfo {
        List<String> fullScreenAds = new ArrayList<>();

        List<String> listAds = new ArrayList<>();

        public AdInfo(List<String> fullScreenAds, List<String> listAds) {
            this.fullScreenAds = fullScreenAds;
            this.listAds = listAds;
        }

        public List<String> getFullScreenAds() {
            return fullScreenAds;
        }

        public void setFullScreenAds(List<String> fullScreenAds) {
            this.fullScreenAds = fullScreenAds;
        }

        public List<String> getListAds() {
            return listAds;
        }

        public void setListAds(List<String> listAds) {
            this.listAds = listAds;
        }
    }

    public static void main(String[] args) {

        HashMap<String, Game> mGameHashMap = new HashMap<>();


        for (Pair p1 : mAllGames) {

            List<String> fullScreenAds = new ArrayList<>();

            List<String> listAds = new ArrayList<>();

            for (Pair p2 : mAllGames) {
                if (!p1.first.equals(p2.first)) {
                    fullScreenAds.add((String) p2.second);
                    listAds.add((String) p2.second);
                }
            }

            GameInfo gameInfo = new GameInfo(p1.second.toString(), p1.first.toString());
            AdInfo adInfo = new AdInfo(fullScreenAds, listAds);

            String pname = (String) p1.second;
            pname = pname.replaceAll("\\.", "_");
            mGameHashMap.put(pname, new Game(gameInfo, adInfo));
        }


        System.out.println(new Gson().toJson(mGameHashMap));
    }
}
