package com.puissantapps.xpromo;

class Constants {

    public static final String EXIT_AD_NONE = "none";
    public static final String EXIT_AD_ICONS = "icons";
    public static final String EXIT_AD_FULL = "full";
    static final String CONTAINER = "http://92919ce15908c00bc323-a8a5d0645fdd989ae568df0fb00208d6.r14.cf5.rackcdn.com/";

    static final String CAMPAIGN_EXIT_ICON_AD = "exit_dialog";
    static final String CAMPAIGN_MORE_GAMES = "more_games";
    static final String CAMPAIGN_FULL_AD = "full_ad";
}
