package com.puissantapps.xpromo;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Handler;
import android.os.Message;
import android.support.annotation.IntDef;
import android.util.Log;

import com.puissantapps.xpromo.model.AdInfo;
import com.puissantapps.xpromo.model.FullAd;
import com.puissantapps.xpromo.model.IconAd;
import com.squareup.picasso.Picasso;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.util.Collections;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.http.GET;
import retrofit2.http.Path;

import static com.puissantapps.xpromo.Utils.getFullAdImageUrl;

public final class Infinity {


    public static final int PORTRAIT = 0;
    public static final int LANDSCAPE = 1;
    private static final String TAG = "Infinity";
    private static final int LAUNCH_FULL_AD = 101;
    private static Infinity mInfinity;

    private final Context mContext;
    private final String mPackageName;
    private final int mOrientation;
    private AdInfo mAdInfo;
    private Retrofit retrofit = new Retrofit.Builder()
            .baseUrl(Constants.CONTAINER)
            .addConverterFactory(GsonConverterFactory.create())
            .build();
    private CrossPromoService mCrossPromoService = retrofit.create(CrossPromoService.class);

    @SuppressLint("HandlerLeak")
    private Handler mHandler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);
            if (msg.what == LAUNCH_FULL_AD) {
                long current = System.currentTimeMillis();

                if (current - mFullScreenAdRequestTime > 3000) return;

                if (mAdInfo != null && mAdInfo.getFullAds() != null && mAdInfo.getFullAds().size() > 0) {
                    launchFullScreenAd();
                } else {
                    mHandler.sendEmptyMessageDelayed(LAUNCH_FULL_AD, 250);
                }
            }
        }
    };
    private long mFullScreenAdRequestTime;
    private boolean mDebuggable;

    private Infinity(Context context, int orientation) {
        this.mContext = context.getApplicationContext();
        this.mPackageName = context.getPackageName();
        this.mOrientation = orientation;

        fetchAdInfo();
    }

    public static void initialize(Context context, @Orientation int orientation) {
        if (context == null) {
            throw new RuntimeException("Context cannot be null");
        }
        mInfinity = new Infinity(context, orientation);
    }

    public static Infinity getInstance() {
        if (mInfinity == null) {
            throw new RuntimeException("Must initialize before using");
        }
        return mInfinity;
    }

    public void setDebuggable(boolean debug) {
        this.mDebuggable = debug;
    }

    AdInfo getAdInfo() {
        return mAdInfo;
    }

    private void fetchAdInfo() {

        Call<AdInfo> adInfoCall = mCrossPromoService.getAdInfo(mPackageName);

        adInfoCall.enqueue(new Callback<AdInfo>() {
            @Override
            public void onResponse(Call<AdInfo> call, Response<AdInfo> response) {

                if (response != null) {
                    mAdInfo = response.body();

                    if (mAdInfo != null && mAdInfo.getFullAds().size() > 0) {
                        List<FullAd> fullAds = Utils.removeInstalledAppsFullAds(mAdInfo.getFullAds(), mContext);
                        if (mAdInfo.getFullAds().size() > 0 && mAdInfo.isShuffleFullAds()) {
                            Collections.shuffle(fullAds);
                        }
                        mAdInfo.setFullAds(fullAds);
                    }

                    String bgUrl = getFullAdBgUrl();
                    if (bgUrl != null) {
                        Picasso.with(mContext).load(bgUrl).priority(Picasso.Priority.HIGH);
                    }

                    List<IconAd> iconAds = mAdInfo != null ? mAdInfo.getIconAds() : null;
                    if (iconAds != null && iconAds.size() > 0) {
                        for (IconAd iconAd : iconAds) {
                            String iconUrl = Utils.getImageIconUrl(iconAd.getPackageName());
                            if (iconUrl != null) Picasso.with(mContext).load(iconUrl);
                        }
                    }
                }

                if (mDebuggable) Log.d(TAG, "onResponse: mAdInfo " + mAdInfo);

            }

            @Override
            public void onFailure(Call<AdInfo> call, Throwable t) {
            }
        });
    }


    private void launchFullScreenAd() {

        if (mAdInfo != null && mAdInfo.getFullAds().size() > 0) {
            List<FullAd> fullAds = Utils.removeInstalledAppsFullAds(mAdInfo.getFullAds(), mContext);
            if (mAdInfo.getFullAds().size() > 0 && mAdInfo.isShuffleFullAds()) {
                Collections.shuffle(fullAds);
            }
            mAdInfo.setFullAds(fullAds);
        }


        if (mAdInfo != null && mAdInfo.getFullAds() != null && mAdInfo.getFullAds().size() > 0) {

            String bgUrl = getFullAdBgUrl();
            if (bgUrl != null) {
                Picasso.with(mContext).load(bgUrl).priority(Picasso.Priority.HIGH).fetch(new com.squareup.picasso.Callback() {
                    @Override
                    public void onSuccess() {
                        long current = System.currentTimeMillis();
                        if (current - mFullScreenAdRequestTime > 3000) return;

                        Intent intent = new Intent(mContext, FullAdActivity.class);
                        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        mContext.startActivity(intent);
                    }

                    @Override
                    public void onError() {

                    }
                });
            }


        }


    }


    public boolean showExitDialog(Activity activity) {
        if (mAdInfo != null && !Constants.EXIT_AD_NONE.equals(mAdInfo.getExitAd())
                && mAdInfo.getIconAds() != null && mAdInfo.getIconAds().size() > 0) {
            DialogHelper.showDialog(activity);
            return true;
        }
        return false;
    }

    public void showFullScreenAd() {
        if (mAdInfo != null && mAdInfo.getFullAds() != null && mAdInfo.getFullAds().size() > 0) {
            mFullScreenAdRequestTime = System.currentTimeMillis();
            launchFullScreenAd();
        } else {
            mFullScreenAdRequestTime = System.currentTimeMillis();
            mHandler.sendEmptyMessage(LAUNCH_FULL_AD);
        }
    }

    public boolean showMoreGames() {
        if (mAdInfo != null && mAdInfo.getIconAds() != null && mAdInfo.getIconAds().size() > 0) {
            Intent intent = new Intent(mContext, MoreGamesActivity.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            mContext.startActivity(intent);
            return true;
        }
        return false;
    }

    final String getFullAdBgUrl() {

        if (mAdInfo != null && mAdInfo.getFullAds().size() > 0) {
            FullAd fullAd = mAdInfo.getFullAds().get(0);
            return getFullAdImageUrl(fullAd.getPackageName(), mOrientation);
        }

        return null;
    }


    int getOrientation() {
        return mOrientation;
    }

    public boolean isDebuggable() {
        return mDebuggable;
    }

    @IntDef({PORTRAIT, LANDSCAPE})
    @Retention(RetentionPolicy.SOURCE)
    public @interface Orientation {
    }

    public interface CrossPromoService {
        @GET("getAds/{packageName}.json")
        Call<AdInfo> getAdInfo(@Path("packageName") String packageName);
    }
}
