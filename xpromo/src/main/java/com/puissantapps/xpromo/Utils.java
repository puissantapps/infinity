package com.puissantapps.xpromo;

import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.util.Log;

import com.puissantapps.xpromo.model.FullAd;
import com.puissantapps.xpromo.model.IconAd;

import java.util.ArrayList;
import java.util.List;

import static com.puissantapps.xpromo.Constants.CONTAINER;

public class Utils {


    static String getFullAdImageUrl(String fetchPackage, @Infinity.Orientation int orientation) {
        return orientation == Infinity.LANDSCAPE ?
                getLandscapeBGUrl(fetchPackage) : getPortraitBGUrl(fetchPackage);
    }

    private static String getPortraitBGUrl(String fetchPackage) {
        return CONTAINER + "res/" + fetchPackage + "/320x480.jpg";
    }


    private static String getLandscapeBGUrl(String fetchPackage) {
        return CONTAINER + "res/" + fetchPackage + "/480x320.jpg";
    }


    static String getImageIconUrl(String fetchPackage) {
        return CONTAINER + "res/" + fetchPackage + "/icon.jpg";
    }


    static void launchStore(String appPackageName, Context context, String campaign) {

        String source = "papps_";
        //https://play.google.com/store/apps/details?id=mygame&referrer=utm_source=papps&utm_medium=banner
        try {
            source = source + getSource(context);
            Intent i = new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=" + appPackageName + "&referrer=utm_source=" + source + "&utm_campaign=" + campaign));
            i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            context.startActivity(i);
        } catch (android.content.ActivityNotFoundException anfe) {
            Intent i = new Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/details?id=" + appPackageName + "&referrer=utm_source=" + source + "&utm_campaign=" + campaign));
            i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            context.startActivity(i);
        }

        if (Infinity.getInstance().isDebuggable()) {
            Log.d("Launch", "launchStore: " + source);
            Log.d("Launch", "launchStore: " + campaign);
        }
    }

    static boolean isAppInstalled(String packageName, Context context) {
        PackageManager pm = context.getPackageManager();
        boolean appInstalled;
        try {
            pm.getPackageInfo(packageName, PackageManager.GET_ACTIVITIES);
            appInstalled = true;
        } catch (PackageManager.NameNotFoundException e) {
            appInstalled = false;
        }
        return appInstalled;
    }

    static List<IconAd> removeInstalledAppsFromIconAds(List<IconAd> iconAds, Context context) {
        List<IconAd> filteredApps = new ArrayList<>();
        for (IconAd ad : iconAds) {
            if (!isAppInstalled(ad.getPackageName(), context)) {
                filteredApps.add(ad);
            }
        }
        return filteredApps;
    }

    static List<FullAd> removeInstalledAppsFullAds(List<FullAd> iconAds, Context context) {
        List<FullAd> filteredApps = new ArrayList<>();
        for (FullAd ad : iconAds) {
            if (!isAppInstalled(ad.getPackageName(), context)) {
                filteredApps.add(ad);
            }
        }
        return filteredApps;
    }


    private static String getSource(Context context) {
        String source = context.getApplicationInfo().loadLabel(context.getPackageManager()).toString();
        source = source.replaceAll(" ", "");
        return source;
    }

}
