package com.puissantapps.xpromo;

import android.app.Activity;
import android.content.pm.ActivityInfo;
import android.graphics.Color;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;

import com.puissantapps.xpromo.model.AdInfo;
import com.puissantapps.xpromo.model.FullAd;
import com.squareup.picasso.Picasso;

import java.util.List;

public class FullAdActivity extends Activity {

    private FullAd mFullAd;
    private View.OnClickListener mYesClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            Utils.launchStore(mFullAd.getPackageName(), FullAdActivity.this, Constants.CAMPAIGN_FULL_AD);
            finish();
        }
    };
    private View.OnClickListener mNoClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            finish();
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        final int activityOrientation = Infinity.getInstance().getOrientation() == Infinity.PORTRAIT ?
                ActivityInfo.SCREEN_ORIENTATION_PORTRAIT : ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE;
        setRequestedOrientation(activityOrientation);

        super.onCreate(savedInstanceState);
        setContentView(R.layout.full_ad_ui);


        AdInfo adInfo = Infinity.getInstance().getAdInfo();
        List<FullAd> fullAds = adInfo != null ? adInfo.getFullAds() : null;

        if (fullAds == null || fullAds.isEmpty()) {
            finish();
            return;
        }

        mFullAd = fullAds.get(0);

        if (mFullAd == null) {
            finish();
            return;
        }


        ImageView adBg = (ImageView) findViewById(R.id.fullAdImg);
        Picasso.with(this)
                .load(Infinity.getInstance().getFullAdBgUrl())
                .into(adBg);

        adBg.setOnClickListener(mYesClickListener);

        View playBtn = findViewById(R.id.playBtn);
        playBtn.setBackgroundColor(Color.parseColor(mFullAd.getButtonColor()));
        playBtn.setOnClickListener(mYesClickListener);

        View yesBtn = findViewById(R.id.yesIcon);
        yesBtn.setOnClickListener(mYesClickListener);

        View noBtn = findViewById(R.id.noIcon);
        noBtn.setOnClickListener(mNoClickListener);


    }


}
