package com.puissantapps.xpromo;

import android.app.Activity;
import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;

import com.puissantapps.xpromo.model.AdInfo;
import com.puissantapps.xpromo.model.IconAd;

import java.util.Collections;
import java.util.List;

public class MoreGamesActivity extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        final int activityOrientation = Infinity.getInstance().getOrientation() == Infinity.PORTRAIT ?
                ActivityInfo.SCREEN_ORIENTATION_PORTRAIT : ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE;
        setRequestedOrientation(activityOrientation);

        AdInfo adInfo = Infinity.getInstance().getAdInfo();
        if (adInfo == null) {
            finish();
            return;
        }

        setContentView(R.layout.activity_more_games);

        RecyclerView recyclerView = (RecyclerView) findViewById(R.id.moreGamesRecyclerView);

        GridLayoutManager layoutManager = new GridLayoutManager(this, 2);
        recyclerView.setLayoutManager(layoutManager);

        List<IconAd> iconAds = Utils.removeInstalledAppsFromIconAds(adInfo.getIconAds(), this);

        if (adInfo.isShuffleIconAds())
            Collections.shuffle(iconAds);


        IconsAdapter iconsAdapter = new IconsAdapter(iconAds, new IconsAdapter.OnIconAdClickListener() {
            @Override
            public void onItemClick(@NonNull IconAd item) {
                Utils.launchStore(item.getPackageName(), MoreGamesActivity.this, Constants.CAMPAIGN_MORE_GAMES);
                finish();
            }
        });
        recyclerView.setAdapter(iconsAdapter);
    }


}
