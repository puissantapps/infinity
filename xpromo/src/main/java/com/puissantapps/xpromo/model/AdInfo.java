
package com.puissantapps.xpromo.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

public class AdInfo {

    @SerializedName("enabled")
    @Expose
    private boolean enabled;

    @SerializedName("shuffleIconAds")
    @Expose
    private boolean shuffleIconAds;

    @SerializedName("shuffleFullAds")
    @Expose
    private boolean shuffleFullAds;

    @SerializedName("packageName")
    @Expose
    private String packageName;

    @SerializedName("exitAd")
    @Expose
    private String exitAd;

    @SerializedName("fullAds")
    @Expose
    private List<FullAd> fullAds = new ArrayList<FullAd>();

    @SerializedName("iconAds")
    @Expose
    private List<IconAd> iconAds = new ArrayList<IconAd>();

    /**
     * No args constructor for use in serialization
     */
    public AdInfo() {
    }

    /**
     * @param iconAds
     * @param shuffleFullAds
     * @param enabled
     * @param packageName
     * @param exitAd
     * @param shuffleIconAds
     * @param fullAds
     */
    public AdInfo(boolean enabled, boolean shuffleIconAds, boolean shuffleFullAds, String packageName, String exitAd, List<FullAd> fullAds, List<IconAd> iconAds) {
        this.enabled = enabled;
        this.shuffleIconAds = shuffleIconAds;
        this.shuffleFullAds = shuffleFullAds;
        this.packageName = packageName;
        this.exitAd = exitAd;
        this.fullAds = fullAds;
        this.iconAds = iconAds;
    }

    /**
     * @return The enabled
     */
    public boolean isEnabled() {
        return enabled;
    }

    /**
     * @param enabled The enabled
     */
    public void setEnabled(boolean enabled) {
        this.enabled = enabled;
    }

    /**
     * @return The shuffleIconAds
     */
    public boolean isShuffleIconAds() {
        return shuffleIconAds;
    }

    /**
     * @param shuffleIconAds The shuffleIconAds
     */
    public void setShuffleIconAds(boolean shuffleIconAds) {
        this.shuffleIconAds = shuffleIconAds;
    }

    /**
     * @return The shuffleFullAds
     */
    public boolean isShuffleFullAds() {
        return shuffleFullAds;
    }

    /**
     * @param shuffleFullAds The shuffleFullAds
     */
    public void setShuffleFullAds(boolean shuffleFullAds) {
        this.shuffleFullAds = shuffleFullAds;
    }

    /**
     * @return The packageName
     */
    public String getPackageName() {
        return packageName;
    }

    /**
     * @param packageName The packageName
     */
    public void setPackageName(String packageName) {
        this.packageName = packageName;
    }

    /**
     * @return The exitAd
     */
    public String getExitAd() {
        return exitAd;
    }

    /**
     * @param exitAd The exitAd
     */
    public void setExitAd(String exitAd) {
        this.exitAd = exitAd;
    }

    /**
     * @return The fullAds
     */
    public List<FullAd> getFullAds() {
        return fullAds;
    }

    /**
     * @param fullAds The fullAds
     */
    public void setFullAds(List<FullAd> fullAds) {
        this.fullAds = fullAds;
    }

    /**
     * @return The iconAds
     */
    public List<IconAd> getIconAds() {
        return iconAds;
    }

    /**
     * @param iconAds The iconAds
     */
    public void setIconAds(List<IconAd> iconAds) {
        this.iconAds = iconAds;
    }

    @Override
    public String toString() {
        return "AdInfo{" +
                "enabled=" + enabled +
                ", shuffleIconAds=" + shuffleIconAds +
                ", shuffleFullAds=" + shuffleFullAds +
                ", packageName='" + packageName + '\'' +
                ", exitAd='" + exitAd + '\'' +
                ", fullAds=" + fullAds +
                ", iconAds=" + iconAds +
                '}';
    }
}
