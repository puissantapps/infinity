
package com.puissantapps.xpromo.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class FullAd {

    @SerializedName("packageName")
    @Expose
    private String packageName;

    @SerializedName("buttonColor")
    @Expose
    private String buttonColor;

    /**
     * No args constructor for use in serialization
     */
    public FullAd() {
    }

    public FullAd(String packageName, String buttonColor) {
        this.packageName = packageName;
        this.buttonColor = buttonColor;
    }

    /**
     * @return The packageName
     */
    public String getPackageName() {
        return packageName;
    }

    /**
     * @param packageName The packageName
     */
    public void setPackageName(String packageName) {
        this.packageName = packageName;
    }

    /**
     * @return The buttonColor
     */
    public String getButtonColor() {
        return buttonColor;
    }

    /**
     * @param buttonColor The buttonColor
     */
    public void setButtonColor(String buttonColor) {
        this.buttonColor = buttonColor;
    }

    @Override
    public String toString() {
        return "FullAd{" +
                "packageName='" + packageName + '\'' +
                ", buttonColor='" + buttonColor + '\'' +
                '}';
    }
}
