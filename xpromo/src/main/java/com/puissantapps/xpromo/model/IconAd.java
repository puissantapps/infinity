
package com.puissantapps.xpromo.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class IconAd {

    @SerializedName("packageName")
    @Expose
    private String packageName;

    @SerializedName("name")
    @Expose
    private String name;

    /**
     * No args constructor for use in serialization
     */
    public IconAd() {
    }

    public IconAd(String packageName, String name) {
        this.packageName = packageName;
        this.name = name;
    }

    /**
     * @return The packageName
     */
    public String getPackageName() {
        return packageName;
    }

    /**
     * @param packageName The packageName
     */
    public void setPackageName(String packageName) {
        this.packageName = packageName;
    }

    /**
     * @return The name
     */
    public String getName() {
        return name;
    }

    /**
     * @param name The name
     */
    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return "IconAd{" +
                "packageName='" + packageName + '\'' +
                ", name='" + name + '\'' +
                '}';
    }
}
