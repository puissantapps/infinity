package com.puissantapps.xpromo;

import android.app.Activity;
import android.app.Dialog;
import android.support.annotation.NonNull;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.Window;
import android.widget.Button;

import com.puissantapps.xpromo.model.AdInfo;
import com.puissantapps.xpromo.model.IconAd;

import java.util.Collections;
import java.util.List;

class DialogHelper {

    public static void showDialog(final Activity activity) {

        AdInfo adInfo = Infinity.getInstance().getAdInfo();
        if (adInfo == null) return;

        final Dialog dialog = new Dialog(activity);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.exit_dialog);

        RecyclerView recyclerView = (RecyclerView) dialog.findViewById(R.id.iconsRecyclerView);

        LinearLayoutManager layoutManager = new LinearLayoutManager(activity, LinearLayoutManager.HORIZONTAL, false);
        layoutManager.setAutoMeasureEnabled(true);
        recyclerView.setLayoutManager(layoutManager);

        Button exitButton = (Button) dialog.findViewById(R.id.btnExit);
        Button cancelButton = (Button) dialog.findViewById(R.id.btnCancel);

        exitButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
                activity.finish();
            }
        });

        cancelButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
            }
        });

        List<IconAd> iconAds = Utils.removeInstalledAppsFromIconAds(adInfo.getIconAds(), activity);

        if (adInfo.isShuffleIconAds())
            Collections.shuffle(iconAds);

        IconsAdapter iconsAdapter = new IconsAdapter(iconAds, new IconsAdapter.OnIconAdClickListener() {
            @Override
            public void onItemClick(@NonNull IconAd item) {
                Utils.launchStore(item.getPackageName(), activity, Constants.CAMPAIGN_EXIT_ICON_AD);
                dialog.dismiss();
            }
        });
        recyclerView.setAdapter(iconsAdapter);


        dialog.show();

    }
}
