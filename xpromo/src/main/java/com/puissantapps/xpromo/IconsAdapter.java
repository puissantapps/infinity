package com.puissantapps.xpromo;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.puissantapps.xpromo.model.IconAd;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class IconsAdapter extends RecyclerView.Adapter<IconsAdapter.ViewHolder> {

    private List<IconAd> mIconAds = new ArrayList<>();

    private OnIconAdClickListener mOnIconAdClickListener;

    public IconsAdapter(List<IconAd> iconAds, OnIconAdClickListener iconAdClickListener) {
        this.mIconAds = iconAds;
        this.mOnIconAdClickListener = iconAdClickListener;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.ad_icons_item, parent, false);


        return new ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {

        holder.setPos(position);
        IconAd iconAd = mIconAds.get(position);
        holder.gameNameText.setText(mIconAds.get(position).getName());
        loadImage(holder.gameIconView, getIconUrl(iconAd.getPackageName()));
    }

    private String getIconUrl(String packageName) {
        return Utils.getImageIconUrl(packageName);
    }

    private void loadImage(ImageView iv, String url) {
        Picasso.with(iv.getContext())
                .load(url)
                .into(iv);
    }

    @Override
    public int getItemCount() {
        return mIconAds == null ? 0 : mIconAds.size();
    }

    public interface OnIconAdClickListener {
        void onItemClick(@NonNull IconAd item);
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        private ImageView gameIconView;

        private TextView gameNameText;

        private int pos;

        public ViewHolder(View itemView) {
            super(itemView);

            gameNameText = (TextView) itemView.findViewById(R.id.txtGameName);
            gameIconView = (ImageView) itemView.findViewById(R.id.gameIcon);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    IconAd iconAd = mIconAds != null ? mIconAds.get(pos) : null;
                    if (mOnIconAdClickListener != null && iconAd != null) {
                        mOnIconAdClickListener.onItemClick(iconAd);
                    }
                }
            });

        }

        public void setPos(int pos) {
            this.pos = pos;
        }
    }
}
